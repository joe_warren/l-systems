#!/usr/bin/env python3
import l_systems
import turtle
import json 

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Generate a folder of images')
    parser.add_argument('definition', type=str, help='json file defining an L-System')
    parser.add_argument('directory', type=str, help='directory to generate images in')
    args = parser.parse_args()
    directory = args.directory
    with open(args.definition) as df:
        definition = json.load(df)
    iterationFrom = definition["iterationFrom"]
    iterationTo = definition["iterationTo"]
    iterationStep = definition.get("iterationStep", 1)
    size = definition.get("size", 150)
    for i, angle in enumerate(definition["angles"]):
        for j, batchsize in enumerate(range(iterationFrom, iterationTo+1, iterationStep)):
            with open("{dir}/{i}-{j}.svg".format(dir=directory, i=i, j=j), "w") as f:
                t = turtle.Turtle()
                system = l_systems.TurtleLSystem(t, angle, 10, definition["substitutions"], definition["start"])
                web = l_systems.TurtleLSystem(t, angle*2, 10, {"F":"-FEE","E":"FF","N":"[N]NF-F+F","K":"F" }, "KKKE")
                system.run(batchsize)
                f.write(t.to_svg(size, size))


    with open("{dir}/index.html".format(dir=directory, i=i, j=j), "w") as f:
        f.write(""""<!doctype html>
            <html>
              <head>
                <title>L-Systems</title>
                <style>
                  body {
                    background-color: black;
                  }
                  table {
                    margin: 5em auto;
                  }
                  td, tr, img {
                    margin: 0;
                    padding: 0;
                    border-width: 0;
                  }
                </style>
              </head>
              <body>
              <table>
              """)

        for i, angle in enumerate(definition["angles"]):
            f.write("""<tr>
                """)
            for j, batchsize in enumerate(range(iterationFrom, iterationTo+1, iterationStep)):
                f.write("""<td>
                    <img src="{i}-{j}.svg"/>
                    </td>
                    """.format(i=i, j=j))
            f.write("""</tr>
                """)

        f.write("""</table></body></html>
        """)
