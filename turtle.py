#!/usr/bin/env python3
import math
class Vec2():
    def __init__(self, x, y):
        self._x = float(x)
        self._y = float(y)
    
    def __add__(self, other):
        return Vec2(self._x + other.x, self._y + other._y)

    def __sub__(self, other):
        return self + (-other)

    def __neg__(self):
        return Vec2(-self._x, -self._y)

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    def __mul__(self, other):
        types = (int, float)
        if isinstance(self, types):
            return Vec2(self * other.x, self * other.y)
        elif isinstance(other, types):
            return Vec2(self._x * other, self._y * other)
        else:
            raise TypeError()

    def __rmul__(self, other):
        return Vec2(other * self._x, other * self._y)

    def rotated(self, theta):
        c = math.cos(theta)
        s = math.sin(theta)
        return Vec2(self._x * c + self._y * s, - self._x * s + self._y * c) 

    def __repr__(self):
        return "{}, {}".format(self._x, self._y)

class Colour():
    def eval(self, percent):
        raise NotImplemented()

class FixedColor(Colour):
    def __init__(self, colour="black"):
        self._colour = colour
    def eval(self, percent):
        return self._colour

class HSV(Colour):
    def eval(self, percent):
        h = percent*360
        s = 1.0
        v = 1.0
        h60 = h / 60.0
        h60f = math.floor(h60)
        hi = int(h60f) % 6
        f = h60 - h60f
        p = v * (1 - s)
        q = v * (1 - f * s)
        t = v * (1 - (1 - f) * s)
        r, g, b = 0, 0, 0
        if hi == 0: r, g, b = v, t, p
        elif hi == 1: r, g, b = q, v, p
        elif hi == 2: r, g, b = p, v, t
        elif hi == 3: r, g, b = p, q, v
        elif hi == 4: r, g, b = t, p, v
        elif hi == 5: r, g, b = v, p, q
        r, g, b = int(r * 255), int(g * 255), int(b * 255)
        return "#%02x%02x%02x" % (r, g, b)


class Turtle():
    def __init__(self):
        self._pos = Vec2(150.0, 150.0)
        self._dir = Vec2(0.0, -1.0)
        self._lines = []
        self._stack = []

    def forward(self, distance):
        next_pos = self._pos + (distance * self._dir)
        self._lines.append((self._pos, next_pos))
        self._pos = next_pos

    def left(self, degrees):
        self._dir = self._dir.rotated(-math.radians(degrees))

    def right(self, degrees):
        self._dir = self._dir.rotated(math.radians(degrees))

    def push(self):
        self._stack.append((self._pos, self._dir))

    def pop(self):
        self._pos, self._dir = self._stack.pop()

    def __repr__(self):
        return str(self._dir) + "@" + "->".join(str(p) for p in self._lines)

    def to_svg(self, width, height, colour=HSV()):
        points = [self._lines[0][0]] + [l[1] for l in self._lines]
        minX = min(p.x for p in points)
        maxX = max(p.x for p in points)
        minY = min(p.y for p in points)
        maxY = max(p.y for p in points)
        pointWidth = maxX - minX
        pointHeight = maxY - minY
        if pointWidth == 0:
            pointWidth = 1
        if pointHeight == 0:
            pointHeight = 1
        scale = min(width/ pointWidth, height/pointHeight)
        offset = Vec2((width-scale*pointWidth), (height-scale*pointHeight))*0.5
        topleft = Vec2(minX, minY)
        def scaleFn(p):
            return scale * (p-topleft) + offset
        lines = ((scaleFn(a), scaleFn(b)) for a, b in self._lines)
        length = len(self._lines)
        def lineString(start, end, percent):
            return """
                <line stroke-width="2" stroke="{colour}"
                x1="{x1}" y1="{y1}" x2="{x2}" y2="{y2}" />
                """.format(
                    x1=start.x, y1=start.y, x2=end.x, y2=end.y, 
                    colour=colour.eval(percent))
        lineNodes = (lineString(a, b, i/length) 
                for (i, (a, b)) in enumerate(lines))

        return """
            <svg xmlns="http://www.w3.org/2000/svg" width="{width}" height="{height}">
                {path}
            </svg>
            """.format(path="".join(lineNodes), width = width, height=height)
