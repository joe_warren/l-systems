#!/usr/bin/env python3
import turtle

class LSystem():
    def __init__(self, symbols, substitutions, start):
        self._symbols = symbols
        self._substitutions = substitutions
        self._start = start


    def substitute(self, string):
        return (y for x in string for y in self._substitutions.get(x, x))

    def run(self, depth):
        string = self._start
        for i in range(0, depth):
            string = self.substitute(string)
        for c in string:
            if c in self._symbols:
                self._symbols[c]()

class TurtleLSystem(LSystem):
    def forward(self):
        self._turtle.forward(self._distance)
    def left(self):
        self._turtle.left(self._angle)
    def right(self):
        self._turtle.right(self._angle)

    def __init__(self, turtle, angle, distance, substitutions, start="F"):
        self._turtle = turtle
        self._angle = angle
        self._distance = distance
        self._symbols = {
            "F": self.forward,
            "G": self.forward,
            "+": self.left,
            "-": self.right, 
            "[": turtle.push,
            "]": turtle.pop
        }
        self._substitutions = substitutions
        self._start = start
if __name__ == "__main__":

    t = turtle.Turtle()
    crack =  TurtleLSystem(t, 45, 10, {"F": "+F--FF++F-"})
    hexagons = TurtleLSystem(t, 60, 10, {"F": "+F-F-F+"})
    sierpinski = TurtleLSystem(t, 120, 10, {"F":"F-G+F+G-F", "G":"GG"}, "F-G-G")
    koch = TurtleLSystem(t, 60, 10, {"F": "F-F++F-F"})
    dragon = TurtleLSystem(t, 90, 10, {"X": "X+YF+", "Y":"-FX-Y"}, "FX")
    tree = TurtleLSystem(t, 45, 10, {"F": "G[-F]+F", "G":"GG"}, "F")
    plant = TurtleLSystem(t, 25, 10, {"X": "F[-X][X]F[-X]+FX", "F":"FF"}, "X")
    plant.run(5)
    print(t.to_svg(1000, 1000))

