L-Systems
=========

Python implementation of Lindenmayer parallel rewrite systems.
Also contains a rough implementation of a turtle graphics system capable of svg output.

## Usage

```    
mkdir out
```

```
./batch.py definitions/plant.json out
```

Then open the index.html file created in the out directory, also explore the other files in the definitions directory, and consider creating your own.
